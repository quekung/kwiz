<?php
//header("HTTP/1.1 301 Moved Permanently"); 
//header("Location: https://www.smarttechcleaning.com/"); 
//exit();
?>
<!DOCTYPE html>
<html lang="en-US" class="no-js">
<head>
<!-- header-top -->
<?php include("incs/header-top.html"); ?>
<!-- /header-top -->
</head>

<body class="home page-template-default page page-id-1">
<!-- header -->
<?php include("incs/header.html"); ?>
<!-- /header -->
  <div class="navbar-pin"></div>

  <main id="ecobot-scrub-50">

  
<section class="hero-subpage hero-center" style="background-image: url('assets/imgs/hero-ecobot-scrub-50-1920x680.jpg');">

	<div class="hero-title">
		<div class="container-fluid">
			<div class="row">
				<div class="col-11 mx-auto">
					<div class="d-flex flex-column align-items-center align-items-xl-start">
						<h1 class="feature-heading mb-2"><img src="assets/imgs/cleaning-e50.png" alt="Scrub 50"></h1>
						<p class="h3 text-center text-md-left text-white m-0">CHANGING<br>
THE WAY<br>
WE CLEAN</p>					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="hero-overlay"></div>

</section>



  <?php /*?><section id="scrub-50-intro" class="section--text-block bg-white p-0 pt-5 pt-md-6">
  
      
    
    <div class="container-fluid">
      <div class="row">
        <div class="col-11 mx-auto">
          
<div class="row no-gutters">
	<div class="col-12 mx-auto">
		<div class="text-center">
			<h1 class="font-weight-bold text-primary text-center text-uppercase mb-4">Next Generation Cleaning Robot</h1>		</div>
	</div>
		<div class="col-12 col-md-10 col-xl-8 mx-auto">
		<div class="text-center">
			<p class="text-center mb-4">Working on a MHIO (minimal human intervention operation) idea, the Scrub 50 will automatically charge its batteries, fill and discharge water and even rinse its dirty tank autonomously through a working station. Coupled with the potential of lift integration and a powerful fleet management system, the Scrub 50 truly is the next generation in cleaning robotics.</p>
		</div>
	</div>
	</div>        </div>
      </div>
    </div>

    
  
  </section>

  
  <section id="scrub-50-video" class="section--video-block bg-white p-0 pb-5 pb-md-6 pt-3">
  
      
    
    <div class="container-fluid">
      <div class="row">
        <div class="col-11 mx-auto">
          

<article class="video-single">
	<div class="row no-gutters">
		<div class="col-12 col-md-10 col-xl-8 mx-auto">
			<a title="Click to Play ECOBOT Scrub 50 &#8211; Next Generation Autonomous Cleaning Robot" class="video-thumbnail" href="https://www.youtube.com/watch?v=kua-BzhtqYg" data-lity>
				<i class="fas fa-fw fa-2x fa-play"></i>
				<img alt="ECOBOT Scrub 50 &#8211; Next Generation Autonomous Cleaning Robot" class="img-fit" src="assets/imgs/ecobot-scrub-50-next-generation-of-cleaning.jpg">
			</a>
		</div>
	</div>
</article><?php */?>


        </div>
      </div>
    </div>

    
  
  </section>

  
  <section id="scrub-50-productive" class="section--text-block-icons-image bg-white p-0">
  
      
    
    <div class="row no-gutters">
      <div class="col-12">
        
<div class="row no-gutters">
	<div class="col-12 col-lg-6 order-lg-2">
					<img class="img-fit" src="assets/imgs/ecobot-scrub-75-front-1.jpg" alt="Ecobot Scrub 75 Front 2">
			</div>
	<div class="col-12 col-lg-5 col-xl-4 m-auto order-lg-1">
		<div class="d-flex flex-column py-4 py-sm-5 px-4 px-sm-5 px-lg-0">
			<h1 class="section-title font-weight-bold text-primary text-uppercase"><span class="text-tertiary"></span> Productive</h1>
        	<p class="mt-2 mb-2">Exceeds the cleaning capabilities of humans and competitor products. Automate workflow and achieve exceptional results through industry-leading design and technology.</p>
<div class="d-flex flex-column py-3 py-sm-0 ml-2">
								<div class="d-flex flex-row align-items-center justify-content-center py-3">
						
						<div class="d-flex fa-2x">
							<span class="fa-layers fa-fw">
								<i class="fas fa-hexagon text-primary" data-fa-transform="grow-9.5 rotate-90"></i>
								<i class="fa-inverse fal fa-vector-square text-white" data-fa-transform="shrink-4"></i>
							</span>
						</div>

						
						<div class="ml-4 mr-auto">

							<h6 class="font-weight-bold text-primary pb-1">Class Leading Speed & Coverage</h6>							<p class="font-weight-semibold text-dark">Covers up to 1200sqm/hr with a 50cm cleaning width.</p>
						</div>
					</div>
			
			
								<div class="d-flex flex-row align-items-center justify-content-center py-3">
						
						<div class="d-flex fa-2x">
							<span class="fa-layers fa-fw">
								<i class="fas fa-hexagon text-primary" data-fa-transform="grow-9.5 rotate-90"></i>
								<i class="fa-inverse fal fa-arrows text-white" data-fa-transform="shrink-4"></i>
							</span>
						</div>

						
						<div class="ml-4 mr-auto">

							<h6 class="font-weight-bold text-primary pb-1">Corner & Edge-to-Edge Cleaning</h6>							<p class="font-weight-semibold text-dark">Cleans as close as 8cm to walls while reaching areas that competitors can’t.</p>
						</div>
					</div>
			
			
								<div class="d-flex flex-row align-items-center justify-content-center py-3">
						
						<div class="d-flex fa-2x">
							<span class="fa-layers fa-fw">
								<i class="fas fa-hexagon text-primary" data-fa-transform="grow-9.5 rotate-90"></i>
								<i class="fa-inverse fal fa-compass  text-white" data-fa-transform="shrink-4"></i>
							</span>
						</div>

						
						<div class="ml-4 mr-auto">

							<h6 class="font-weight-bold text-primary pb-1">Advanced Navigation</h6>							<p class="font-weight-semibold text-dark">Intelligent software directs to assigned areas while avoiding moving and stationary obstacles.</p>
						</div>
					</div>
			
			</div>		</div>
	</div>
</div>      </div>
    </div>

    
  
  </section>

  
  <section id="scrub-50-simple" class="section--text-block-icons-image bg-white p-0">
  
      
    
    <div class="row no-gutters">
      <div class="col-12">
        
<div class="row no-gutters">
	<div class="col-12 col-lg-6 order-lg-1">
					<img class="img-fit" src="assets/imgs/ecobot-scrub-75-perspective.jpg" alt="Ecobot Scrub 75 Perspective">
			</div>
	<div class="col-12 col-lg-5 col-xl-4 m-auto order-lg-2">
		<div class="d-flex flex-column py-4 py-sm-5 px-4 px-sm-5 px-lg-0">
			<h1 class="section-title font-weight-bold text-primary text-uppercase"><span class="text-tertiary"></span> Simple</h1>
        	<p class="mt-2 mb-2">Cutting-edge technology made simple so even the most novice user can understand and operate. Easily train staff to receive and manage shift reports and system updates on easy-to-use interface.</p>
<div class="d-flex flex-column py-3 py-sm-0 ml-2">
								<div class="d-flex flex-row align-items-center justify-content-center py-3">
						
						<div class="d-flex fa-2x">
							<span class="fa-layers fa-fw">
								<i class="fas fa-hexagon text-primary" data-fa-transform="grow-9.5 rotate-90"></i>
								<i class="fa-inverse fal fa-cog text-white" data-fa-transform="shrink-4"></i>
							</span>
						</div>

						
						<div class="ml-4 mr-auto">

							<h6 class="font-weight-bold text-primary pb-1">Fleet Management System</h6>							<p class="font-weight-semibold text-dark">Easily integrate into workplace and monitor performance anytime, anywhere.</p>
						</div>
					</div>
			
			
								<div class="d-flex flex-row align-items-center justify-content-center py-3">
						
						<div class="d-flex fa-2x">
							<span class="fa-layers fa-fw">
								<i class="fas fa-hexagon text-primary" data-fa-transform="grow-9.5 rotate-90"></i>
								<i class="fa-inverse fal fa-bell text-white" data-fa-transform="shrink-4"></i>
							</span>
						</div>

						
						<div class="ml-4 mr-auto">

							<h6 class="font-weight-bold text-primary pb-1">Reports & Updates</h6>							<p class="font-weight-semibold text-dark">Get shift reports with production summary and easily perform system updates.</p>
						</div>
					</div>
			
			
								<div class="d-flex flex-row align-items-center justify-content-center py-3">
						
						<div class="d-flex fa-2x">
							<span class="fa-layers fa-fw">
								<i class="fas fa-hexagon text-primary" data-fa-transform="grow-9.5 rotate-90"></i>
								<i class="fa-inverse fal fa-map-marked text-white" data-fa-transform="shrink-4"></i>
							</span>
						</div>

						
						<div class="ml-4 mr-auto">

							<h6 class="font-weight-bold text-primary pb-1">Autonomous Mapping</h6>							<p class="font-weight-semibold text-dark">Self-constructed mapping of floor plans and automatically localizes itself with working station/charging station.</p>
						</div>
					</div>
			
			</div>		</div>
	</div>
</div>      </div>
    </div>

    
  
  </section>

  
  <section id="scrub-50-efficient" class="section--text-block-icons-image bg-white p-0">
  
      
    
    <div class="row no-gutters">
      <div class="col-12">
        
<div class="row no-gutters">
	<div class="col-12 col-lg-6 order-lg-2">
					<img class="img-fit" src="assets/imgs/ecobot-scrub-75-back-1.jpg" alt="Ecobot Scrub 75 Back 2">
			</div>
	<div class="col-12 col-lg-5 col-xl-4 m-auto order-lg-1">
		<div class="d-flex flex-column py-4 py-sm-5 px-4 px-sm-5 px-lg-0">
			<h1 class="section-title font-weight-bold text-primary text-uppercase"><span class="text-tertiary"></span> Efficient</h1>
        	<p class="mt-2 mb-2">Its low maintenance design and reliable safety features allow for staff to handle less-tedious cleaning duties while the Scrub 50 operates itself.</p>
<div class="d-flex flex-column py-3 py-sm-0 ml-2">
								<div class="d-flex flex-row align-items-center justify-content-center py-3">
						
						<div class="d-flex fa-2x">
							<span class="fa-layers fa-fw">
								<i class="fas fa-hexagon text-primary" data-fa-transform="grow-9.5 rotate-90"></i>
								<i class="fa-inverse fal fa-fingerprint text-white" data-fa-transform="shrink-4"></i>
							</span>
						</div>

						
						<div class="ml-4 mr-auto">

							<h6 class="font-weight-bold text-primary pb-1">Minimal Human Intervention</h6>							<p class="font-weight-semibold text-dark">Liberates employees to handle less-tedious tasks, automatically charges itself, fills and rinses water tank.</p>
						</div>
					</div>
			
			
								<div class="d-flex flex-row align-items-center justify-content-center py-3">
						
						<div class="d-flex fa-2x">
							<span class="fa-layers fa-fw">
								<i class="fas fa-hexagon text-primary" data-fa-transform="grow-9.5 rotate-90"></i>
								<i class="fa-inverse fal fa-wrench text-white" data-fa-transform="shrink-4"></i>
							</span>
						</div>

						
						<div class="ml-4 mr-auto">

							<h6 class="font-weight-bold text-primary pb-1">Low Maintenance</h6>							<p class="font-weight-semibold text-dark">As low as 2 hours/month of general maintenance with 2 to 3 hour battery life.</p>
						</div>
					</div>
			
			
								<div class="d-flex flex-row align-items-center justify-content-center py-3">
						
						<div class="d-flex fa-2x">
							<span class="fa-layers fa-fw">
								<i class="fas fa-hexagon text-primary" data-fa-transform="grow-9.5 rotate-90"></i>
								<i class="fa-inverse fal fa-shield-check text-white" data-fa-transform="shrink-4"></i>
							</span>
						</div>

						
						<div class="ml-4 mr-auto">

							<h6 class="font-weight-bold text-primary pb-1">Safety & Security</h6>							<p class="font-weight-semibold text-dark">Obstacle avoidance using Lidar surveying, 3D depth camera and ultrasonic sensors along with on board camera.</p>
						</div>
					</div>
			
			</div>		</div>
	</div>
</div>      </div>
    </div>

    
  
  </section>

  


</main>

<footer>

	
	
<section id="request-demo" class="bg-blue">
	<div class="row no-gutters">

		<div class="col-12 col-lg-6 col-xl-6 order-2 order-xl-1">
			<div class="bg-pd  py-5 py-md-6 px-4 px-sm-5 px-xl-6 h-100">
				
				<div class="d-flex flex-column h-100 justify-content-center">
					<figure class="text-center"><img src="assets/imgs/ecobot-scrub-50.png" alt="Ecobot Scrub 50"></figure>
				</div>
			</div>
		</div>
		
		<div class="col-12 col-lg-6 col-xl-5 order-1 order-xl-2 m-auto">
			<div class="d-flex flex-column py-5 py-xl-6 px-4 px-sm-5 px-md-6 px-lg-4">
								<h1 class="font-weight-bold text-primary text-center text-uppercase mb-4">Request for Appointment</h1>
								<p class="font-weight-normal text-white mb-4">For more information about the Ecobot, or to see the Ecobot in action, please fill out the form below and we will get in touch with you!</p>
				<div role="form" class="wpcf7" lang="en-US" dir="ltr">
<form action="/#ecobot-scrub-50" method="post" class="wpcf7-form" novalidate="novalidate">
<?php include("incs/contactform.html"); ?>
</form>
				</div>
			</div>
		</div>

	</div>
</section>

<!-- footer -->
<?php include("incs/footer.html"); ?>
<!-- /footer -->
</footer>

<?php include("incs/js_footer.html"); ?>
</body>
</html>