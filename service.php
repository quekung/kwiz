<?php
//header("HTTP/1.1 301 Moved Permanently"); 
//header("Location: https://www.smarttechcleaning.com/"); 
//exit();
?>
<!DOCTYPE html>
<html lang="en-US" class="no-js">
<head>
<!-- header-top -->
<?php include("incs/header-top.html"); ?>
<!-- /header-top -->
</head>

<body class="home page-template-default page page-id-1">
<!-- header -->
<?php include("incs/header.html"); ?>
<!-- /header -->
  <div class="navbar-pin"></div>

  <main id="ecobot-service">


	<section id="carousel" style="margin-bottom: -30px;z-index: 99">
  
	
      <div class="slide-wrapper">
        <div class="slide-content">
         
		 <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item">
	<div class="container row mx-auto">
		  <figure class="hidden-xs col-sm-7 d-flex justify-content-center justify-content-sm-start align-items-end mb-0">
		  	<img class="col-sm-8 img-bottom" src="assets/imgs/product-banner1.png">
		  </figure>
		  <div class="col-sm-5 d-flex flex-column align-items-start justify-content-center h-100">
			  <h1 class="slide-title font-weight-bold text-white mb-2"><img src="assets/imgs/logo-white.png" height="30"><span class="text-hide"></span></h1>
			  <p class="slide-caption font-weight-light text-white mb-4">Meet our Robots<br>Full range of Autonomous Cleaning<br>Robots Services available</p>
				<div class="d-flex flex-column flex-sm-row">
					<a href="javascript:void(0)" class="btn btn-secondary btn-lg af-none" data-toggle="modal" data-target="#booking-form">REQUEST A DEMO</a>
				  </div>
		  </div>
	  </div>
    </div>
    <div class="carousel-item active">
      <div class="container row mx-auto">

		  <div class="col-sm-6 d-flex flex-column align-items-start justify-content-center h-100">
			  <h1 class="slide-title font-weight-bold text-white mb-2">Our Services</h1>
			  <p class="slide-caption font-weight-light text-white mb-4">
Whether you are a company interested to integrate
robotics salutions into your business or a robotics
manufacturer looking for robotics experts to help
distribute your products</p>
				<p class="slide-caption font-weight-light text-white mb-4">
					<big>we are here to work with you!</big>
				  </p>
		  </div>
	  </div>
    </div>
	
    <div class="carousel-item">
      <div class="container row mx-auto">
		  <figure class="hidden-xs col-sm-7 d-flex justify-content-center justify-content-sm-start align-items-end mb-0">
		  	<img class="col-sm-12" src="assets/imgs/product-banner3.png">
		  </figure>
		  <div class="col-sm-5 d-flex flex-column align-items-start justify-content-center h-100">
			  <h1 class="slide-title font-weight-bold text-white mb-2"><img src="assets/imgs/logo-white.png" height="30"><span class="text-hide"></span></h1>
			  <p class="slide-caption font-weight-light text-white mb-4">Contact<br>For more infomation<br>about the Ecobot</p>
	
		  </div>
	  </div>
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
        </div>
        <!--<a class="scroll-prompt" href="javascript:void(0)">
          <span class="mouse"></span>
          <i class="far fa-chevron-down text-white"></i>
        </a>-->
                  
        <video autoplay muted loop class="slide-video" poster="assets/imgs/scrub-50-wall-detecting-sensors.jpg">
          <source src="assets/imgs/1123878774.mp4" type="video/mp4">
          Your browser does not support HTML5 video.
        </video>
                <img class="slide-bg" src="assets/imgs/scrub-50-wall-detecting-sensors.jpg" alt="Cleaning Redefined">
        <div class="slide-overlay"></div>
      </div>

  </section>
 

  
  <section id="service-detail-1" class="section--text-block-icons-image bg-white p-0">
  
      
    
    <div class="row no-gutters">
      <div class="col-12">
        
<div class="row no-gutters pt-4 pb-4">
	<div class="col-12 col-lg-6 text-center">
					<img src="assets/imgs/service-hl.png" alt="Ecobot Service Circle" style="max-width: 80%;margin: 0 auto">
			</div>
	<div class="col-12 col-lg-5 col-xl-4 m-auto order-lg-1">
		<div class="d-flex flex-column py-4 py-sm-5 px-4 px-sm-5 px-lg-0">
			<h1 class="section-title font-weight-bold text-primary text-uppercase">End-To-End robotics solution provider</h1>
        	<p class="mt-3 mb-3"><big>Robots is not just a tools that you buy and use. It is
a long term investment to transform your business.
This requires changing your current business
workflow, train your employees to use the robots
so that it can be fully integrated into your
organization.</big></p>
			<p class="mt-3 mb-3"><big>RAASPAL provides consulting service and solutions
at all stages to help you</big></p>

		</div>
	</div>
</div>      </div>
    </div>

    
  
  </section>
  
  

  
  <section id="service-detail-2" class="section--text-block-icons-image bg-white p-0">
  
      
    
    <div class="row no-gutters">
      <div class="col-12">
        
<div class="row no-gutters bg-light">
	<div class="col-12 col-lg-5 col-xl-4 m-auto order-lg-1">
			<div class="d-flex flex-column py-4 py-sm-5 px-4 px-sm-5 px-lg-0">
					<h1 class="section-title font-weight-bold text-primary text-uppercase mb-4">Choosing robots</h1>
					<big>
					<ul class="list-bull">
						<li class="mb-2">Having trouble trying to find suitable robots?</li>
						<li class="mb-2">Want to reduce the risk?</li>
						<li class="mb-2">Not sure about the specifications?</li>
					</ul>
					</big>
			</div>	
	</div>
	<div class="col-12 col-lg-5 col-xl-4 m-auto order-lg-2">
		<div class="d-flex flex-column py-4 py-sm-5 px-4 px-sm-5 px-lg-0">
			<h1 class="section-title font-weight-bold text-primary text-uppercase mb-4 hidden-xs">&nbsp;</h1>
        	<p class="mt-2 mb-2"><big>We have a team of experts to scout for robots
around the world and assess them before
recommending them to our customers. This helps
our customers to reduce the risk of getting a robot
that does not perform well for the tasks.</big></p>
</div>
	</div>
</div>      </div>
    </div>

    
  
  </section>
  
  <section id="service-detail-3" class="section--text-block-icons-image bg-white p-0">
  
      
    
    <div class="row no-gutters">
      <div class="col-12">
        
<div class="row no-gutters bg-white">
	<div class="col-12 col-lg-5 col-xl-4 mx-auto order-lg-1">
			<div class="d-flex flex-column py-4 py-sm-5 px-4 px-sm-5 px-lg-0">
					<h1 class="section-title font-weight-bold text-primary text-uppercase mb-4">Integrating robotics solution</h1>
					<big>
					<ul class="list-bull">
						<li class="mb-2">The robot does not come with software that
catered to your needs?</li>
						<li class="mb-2">Need to integrate the robotics solution into
your organization's IT systems?
</li>
						<li class="mb-2">Need to improve your current business
workflow with robotics solutions?</li>
					</ul>
					</big>
			</div>	
	</div>
	<div class="col-12 col-lg-5 col-xl-4 mx-auto order-lg-2">
		<div class="d-flex flex-column py-4 py-sm-5 px-4 px-sm-5 px-lg-0">
			
        	<p class="pt-5 mb-2"><big>We have deployed a range of robots starting from
cleaning robots, delivery robots , security robots to
service concierge robots for different industries. In
most cases, the software application of the robots
need to be customized to adapt to our local context.</big></p>
<p class="mt-2 mb-2"><big>
Here in RAAS PAL, we have a team of developers that
can help to perform software customisation or system
integration to suit your needs.</big></p>
<p class="mt-2 mb-2"><big>
Our experience will better serve you in transforming
your business with robotics solution</big></p>
</div>
	</div>
</div>      </div>
    </div>

    
  
  </section>
  
  <section id="service-detail-4" class="section--text-block-icons-image bg-white p-0">
  
      
    
    <div class="row no-gutters">
      <div class="col-12">
        
<div class="row no-gutters bg-light">
	<div class="col-12 col-lg-5 col-xl-4 mx-auto order-lg-1">
			<div class="d-flex flex-column py-4 py-sm-5 px-4 px-sm-5 px-lg-0">
					<h1 class="section-title font-weight-bold text-primary text-uppercase mb-4">Service and Maintenance</h1>
					<big>
					<ul class="list-bull">
						<li class="mb-2">Worry about after sales service?</li>
						<li class="mb-2">Need help to deploy the robots into your
business?</li>
					</ul>
					</big>
			</div>	
	</div>
	<div class="col-12 col-lg-5 col-xl-4 mx-auto order-lg-2">
		<div class="d-flex flex-column py-4 py-sm-5 px-4 px-sm-5 px-lg-0">
        	<p class="pt-5 mb-2"><big>Most of the robots come from overseas. The robot
manufacturers may not have local presence to
support the after-sales maintenance service.
The local distributor may not have the technical
expertise to perform the maintenance services.</big></p>
        	<p class="mt-2 mb-2"><big>
In RAAS PAL, we have a team of robotics expert who
can develop and understands robots. Thus, we can
work with the robotics manufacturer to provide local
service maintenance support</big></p>
</div>
	</div>
</div>      </div>
    </div>

    
  
  </section>

  
  <section id="service-detail-5" class="section--text-block-icons-image bg-white p-0">
  
      
    
    <div class="row no-gutters">
      <div class="col-12">
        
<div class="row no-gutters">
	<div class="col-12 col-md-10 order-sm-2 mx-auto d-flex align-items-end justify-content-center">
					<img src="assets/imgs/fix-footer.png" alt="Ecobot Scrub 75 Back 2" height="300" style="max-width: 70%">
	</div>
	<div class="col-12 col-md-10 mx-auto order-sm-1 text-center">
		<div class="d-flex flex-column py-4 py-sm-5 px-4 px-sm-5 px-lg-0">
			<h1 class="section-title font-weight-bold text-primary text-uppercase mb-2">We have robotics and industry experts</h1>
        	<p class="mt-2 mb-2"><big>Our team is made up robotics experts and industry domain experts. We have assessed and deployed<br class="hidden-xs">
many different type of robots for different industries. We worked closely with robot manufacturer to<br class="hidden-xs">
develop the software application for the robots.</big></p>
<p class="mt-2 mb-2"><big>
With all the experience gained in using robots in the industries, we are qualifed to understand the<br class="hidden-xs">
challenges and has the capability to develop the robot solution. Thus, we can serve both the robot manu-<br class="hidden-xs">
facturer and organization who interested in using robotics solution to solve their problems</big></p>
	</div>
	</div>
</div>      </div>
    </div>

    
  
  </section>

  


</main>

<footer>



<!-- footer -->
<?php include("incs/footer.html"); ?>
<!-- /footer -->
</footer>

<?php include("incs/js_footer.html"); ?>
</body>
</html>