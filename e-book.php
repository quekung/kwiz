<?php
//header("HTTP/1.1 301 Moved Permanently"); 
//header("Location: https://www.smarttechcleaning.com/"); 
//exit();
?>
<!DOCTYPE html>
<html lang="en-US" class="no-js">
<head>
<meta charset="UTF-8">
<title>::: SmartTechProperty by One to One Contacts :::</title>

<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="description" content="OTO contactcenter" />
<meta name="keywords" content="OTO contactcenter" />
<meta name="author" content="OTO contactcenter" />

<link rel='stylesheet' id='theme-style-css'  href='assets/css/style.css' media='all' />
<link rel='stylesheet' id='contact-form-7-css'  href='plugins/contact-form-7/includes/css/styles.css' media='all' />
<link rel='stylesheet' id='cf7cf-style-css'  href='plugins/cf7-conditional-fields/style.css' media='all' />
<link rel='stylesheet' id='theme-css'  href='assets/css/theme.css' media='all' />

<link rel="apple-touch-icon" sizes="180x180" href="assets/imgs/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="assets/imgs/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="assets/imgs/favicon-16x16.png">
<link rel="shortcut icon" href="assets/imgs/index.ico">

<meta name="msapplication-TileColor" content="#0d4568">
<meta name="theme-color" content="#ffffff">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>

<link rel="stylesheet" type="text/css" href="assets/css/flipbook.style.css">
<link rel="stylesheet" type="text/css" href="assets/css/font-awesome.css">

<script src="assets/js/flipbook.min.js"></script>

<script type="text/javascript">

    $(document).ready(function () {
        $("#container").flipBook({
            pdfUrl:"assets/files/cleaning-robot-6.pdf",
             btnSearch: {
                enabled: true,
                title: "Search",
                icon: "fas fa-search"

            }
        });

    })
</script>

</head>

<body>
	<div id="container"></div>
</body>
</html>