$(function(){
    $('.wpcf7-form').each(function(){
        $(this).validate({
            errorClass: 'error-contact',
            errorPlacement: function(error, element) {
                show_notie(2, 'One or more fields have an error. Please check and try again.');
            },
            submitHandler: function(form){
            var refer_section_id = $(form).attr('action').substring(2);
            $(form).find('.relate').val(refer_section_id);
            $(form).find('.wpcf7-submit').prop('disabled', true);
            $.post('sendmail.php', $(form).serialize(), function(data){
                    form.reset();
                    $(form).find('.wpcf7-submit').prop('disabled', false);
                    show_notie(3, 'Thank you for your request. We will get back to you shortly.');
            }, 'json');
            }
        });
    });
});