<?php
//header("HTTP/1.1 301 Moved Permanently"); 
//header("Location: https://www.smarttechcleaning.com/"); 
//exit();
?>
<!DOCTYPE html>
<html lang="en-US" class="no-js">
<head>
<!-- header-top -->
<?php include("incs/header-top.html"); ?>
<!-- /header-top -->
</head>

<body class="home page-template-default page page-id-1">
<!-- header -->
<?php include("incs/header.html"); ?>
<!-- /header -->
  <div class="navbar-pin"></div>

  <main id="home">

  
    
    
    
<section id="carousel">
  
	
      <div class="slide-wrapper">
        <div class="slide-content">
         
		 <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
	<div class="container row mx-auto">
		  <figure class="hidden-xs col-sm-7 d-flex justify-content-center justify-content-sm-start align-items-end mb-0">
		  	<img class="col-sm-8 img-bottom" src="assets/imgs/product-banner1.png">
		  </figure>
		  <div class="col-sm-5 d-flex flex-column align-items-start justify-content-center h-100">
			  <h1 class="slide-title font-weight-bold text-white mb-2">K-WIZ SOLUTION</h1>
			  <p class="slide-caption font-weight-light text-white mb-4"><big>แตกต่าง ด้วยความเป็นมืออาชีพ</big></p>
				<div class="d-flex flex-column flex-sm-row">
					<a href="javascript:void(0)" class="btn btn-danger btn-lg af-none" data-toggle="modal" data-target="#booking-form">ติดต่อใช้บริการ</a>
				  </div>
		  </div>
	  </div>
    </div>
    <div class="carousel-item">
      <div class="container row mx-auto">

		  <div class="col-sm-8 d-flex flex-column align-items-start justify-content-center h-100">
			  <h1 class="slide-title font-weight-normal text-white mb-4">ผู้เชี่ยวชาญในด้านงานท่อ</h1>
			  <p class="slide-caption font-weight-light text-white mb-0">
				ให้บริการบำรุงรักษา ระบบสุขาภิบาล รวมทั้งตรวจเช็คสภาพระบบท่อ</p>
				<p class="slide-caption font-weight-light text-white mb-4">
					<big>ด้วยเครื่องมือคุณภาพสูงสำหรับงานท่อ แบบมืออาชีพ</big>
				  </p>
		  </div>
	  </div>
    </div>
	
    <div class="carousel-item">
      <div class="container row mx-auto">
		  <figure class="hidden-xs col-sm-7 d-flex justify-content-center justify-content-sm-start align-items-end mb-0">
		  	<img class="col-sm-12" src="assets/imgs/product-banner3.png">
		  </figure>
		  <div class="col-sm-5 d-flex flex-column align-items-start justify-content-center h-100">
			  <h1 class="slide-title font-weight-normal text-white mb-2">บริการของเรา</h1>
			  <p class="slide-caption font-weight-light text-white mb-4">
			  	<big>แก้ปัญหา<b class="font-weight-normal">ท่อตัน</b></big><br>
				<big>ปรึกษาแก้ไข<b class="font-weight-normal">ท่อรั่วซึม</b></big><br>
				<big><b class="font-weight-normal">บำรุงรักษา</b> ท่อสุขาภิบาล</big><br>
				<big><b class="font-weight-normal">ล้างถังพักน้ำ</b> ล้างถังเก็บน้ำ</big>
			  </p>
	
		  </div>
	  </div>
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
        </div>
        <!--<a class="scroll-prompt" href="javascript:void(0)">
          <span class="mouse"></span>
          <i class="far fa-chevron-down text-white"></i>
        </a>-->
                  
        <video autoplay muted loop class="slide-video" poster="assets/imgs/cover-vdo.jpg">
          <source src="assets/imgs/K-Wiz_Solution.mp4" type="video/mp4">
          Your browser does not support HTML5 video.
        </video>
                <img class="slide-bg" src="assets/imgs/cover-vdo.jpg" alt="K-WIZ SOLUTION">
        <div class="slide-overlay"></div>
      </div>

  </section>


    <section id="product-showcase" class="bg-none">
	<div class="slides container">
		<div class="d-flex flex-column justify-content-center px-0 px-xl-5 h-100 text-center">
				<h3 class="h3 font-weight-semibold text-center">เราคือใคร?</h3>
				<p class="pt-3 mx-auto mx-lg-0">
					<big>ผู้เชี่ยวชาญในด้านงานท่อจัดจำหน่ายเครื่องมือคุณภาพสูงสำหรับงานท่อ และ <br>
					ให้บริการบำรุงรักษา ระบบสุขาภิบาล รวมทั้งตรวจเช็คสภาพระบบท่อ แบบมืออาชีพ</big>
				  </p>
				  <div class="t-big-hl wow fadeInUp mt-3 d-flex justify-content-center" data-wow-delay="0.3s">
					<ul class="list text-left col-md-10">
					<li>เพราะเรา ทราบดีว่า เครื่องมือที่ดีช่วยให้ได้งานที่ดี เราจึง คัดสรรสินค้าเครื่องมือคุณภาพสูงสำหรับงานท่อมาจำหน่าย&nbsp; เพื่อตอบสนองความต้องการในการแก้ปัญหาสำหรับ ช่างมืออาชีพ</li>
					<li>เพราะเรา ให้บริการ บำรุงรักษาท่อสุขาภิบาล เช่น แก้ท่อตัน&nbsp;ท่อรั่วซึม สำหรับ อาคารธุรกิจการค้า, สำนักงาน, โรงเรียน, โรงพยาบาลตลอดจน บ้านพักอาศัย โดยวิศวกรที่มีประสบการณ์ในงานระบบอาคาร และในบริษัทผู้ผลิตเครื่องมืออันทันสมัยสำหรับแก้ไขปัญหาระบบงานท่อโดยเฉพาะ</li>
					<li>เพราะเรา เข้าใจในความต้องการ และทราบถึงความจำเป็นเร่งด่วนของลูกค้าจึงออกแบบบริการของเราเพื่อตอบสนองความพึงพอใจของลูกค้า ซึ่ง เป็นสิ่งที่สำคัญที่สุดสำหรับเรา</li>
					<li>เพราะเรา เป็นบริษัทที่มีความต้องการที่จะเป็นผู้นำในด้านการให้คำปรึกษาและให้บริการบำรุงรักษาระบบท่อสุขาภิบาล อย่างมืออาชีพโดยเครื่องมือที่ทันสมัย และ ได้รับความไว้วางใจมากที่สุดในประเทศไทย</li>
					</ul>
				</div>
		 </div>
	</div>
	</section>
	
	<section id="product-list" class="bg-light pt-5 pb-5">
	<div class="container">
		<div class="d-flex flex-column justify-content-center h-100 text-center mb-4">
				<h3 class="h3 font-weight-semibold text-center">บริการของเรา</h3>
				<p class="pt-3 mx-auto mx-lg-0">K-WIZ SOLUTION</p>
		 </div>
		 <ul class="d-flex justify-content-between align-items-top list-inline">
		 	<li class="col-sm-4">
			<a href="ecobot-scrub-50.php" title="EBOT SCRUB 50" class="d-block p-2 bg-white rounded">
				<figure><img src="assets/imgs/sv-2.png"><!--<img src="https://www.k-wiz.co.th/static/di/pipe1.png">--></figure>
				<h3 class="text-center">รับเหมาบำรุงรักษาระบบสุขาภิบาล</h3>
				<div class="text-center">เพราะเราเข้าใจในความต้องการ และทราบถึงความจำเป็นเร่งด่วนของลูกค้า ออกแบบบริการของเราเพื่อตอบสนองความพึงพอใจของลูกค้า</div>
			</a>
			</li>
			
			<li class="col-sm-4">
			<a href="ecobot-scrub-50.php" title="EBOT SCRUB 75" class="d-block p-2 bg-white rounded">
				<figure><img src="assets/imgs/sv-3.png"><!--<img src="https://www.k-wiz.co.th/static/di/pipes-red.png">--></figure>
				<h3 class="text-center">รับส่องกล้องสำรวจภายในท่อ</h3>
				<div class="text-center">รับสอบสภาพภายในท่อก่อนตรวจรับงาน หรือ ตรวจหาสาเหตุของปัญหาที่เกิดขึ้นภายในท่อ ทีดีกว่าการคาดเดา และ วางแผนในการทำงานที่ถูกต้อง</div>
			</a>
			</li>
			
			<li class="col-sm-4">
			<a href="#" title="EBOT SWEEP 40" class="d-block p-2 bg-white rounded">
				<figure><img src="assets/imgs/sv-4.png"><!--<img src="https://www.k-wiz.co.th/static/di/pool-red1.png">--></figure>
				<h3 class="text-center">รับล้างถังพักน้ำ ล้างถังเก็บน้ำ</h3>
				<div class="text-center">ปรกติอาคารที่มีการใช้นำเป็นจำนวนมากในเวลาพร้อมๆกันต้องมีถังสำรองน้ำไว้ใช้เพราะ น้ำที่จ่ายมาจากการประปาอาจจะไม่เพียงพอต่อการใช้งาน</div>
			</a>
			</li>
		 </ul>
	</div>
	</section>
  
  <!--<section id="product-showcase" class="section--carousel-showcase bg-image py-5">
  
      
    
    <div class="row no-gutters">
      <div class="col-12">
        
  <div class="slides">

	
	
    <div>
      <div class="row">
        <div class="col-11 col-sm-12 col-md-8 col-lg-4 order-2 order-lg-1 mx-auto">
          <div class="d-flex align-items-center justify-content-center h-100">
		  	<div class="px-0 px-xl-5 mb-3 mb-lg-0">
				<img class="mb-2" src="assets/imgs/cleaning-e75.png" alt="ECOBOT SCRUB 75">
           		<h1 class="font-weight-bold text-uppercase">TAKES THE <br>
GUESSWORK OUT<br>
OF CLEANING</h1>
</div>
          </div>
        </div>
        <div class="col-12 col-lg-3 order-1 order-lg-2 mb-5 mb-lg-0">
          <img src="assets/imgs/ecobot-scrub-75.png" alt="Ecobot Scrub 75" class="img-fluid mx-auto">
        </div>
        <div class="col-11 col-sm-9 col-md-7 col-lg-4 order-3 order-lg-3 mx-auto">
          <div class="d-flex flex-column justify-content-center px-0 px-xl-5 h-100">
            <p class="h5 font-weight-semibold text-center text-lg-left">Using its 3 sensor systems, LIDAR, 3-D depth
cameras and Ultrasonic sensors, works to attain a
99% consistent coverage area.</p>
                        <div class="pt-3 mx-auto mx-lg-0">
                            <a href="e-book.php" class="btn btn-lg btn-secondary" target="_blank">Download Brochure</a>
                          </div>
                      </div>
        </div>
      </div>
    </div>

    <div>
      <div class="row">
        <div class="col-11 col-sm-12 col-md-8 col-lg-4 order-2 order-lg-1 mx-auto">
          <div class="d-flex align-items-center justify-content-center h-100">
		  	<div class="px-0 px-xl-5 mb-3 mb-lg-0">
				<img class="mb-2" src="assets/imgs/cleaning-e50.png" alt="ECOBOT SCRUB 75">
            	<h1 class="font-weight-bold text-uppercase">CHANGING <br>
THE WAY<br>
WE CLEAN</h1>
			</div>
          </div>
        </div>
        <div class="col-12 col-lg-3 order-1 order-lg-2 mb-5 mb-lg-0">
          <img src="assets/imgs/ecobot-scrub-50.png" alt="Ecobot Scrub 50" class="img-fluid mx-auto">
        </div>
        <div class="col-11 col-sm-9 col-md-7 col-lg-4 order-3 order-lg-3 mx-auto">
          <div class="d-flex flex-column justify-content-center px-0 px-xl-5 h-100">
            <p class="h5 font-weight-semibold text-center text-lg-left">Specializing in automation, accountability and
efficiency on a global scale.</p>
                        <div class="pt-3 mx-auto mx-lg-0">
                            <a href="e-book.php" class="btn btn-lg btn-secondary" target="_blank">Download Brochure</a>
                          </div>
                      </div>
        </div>
      </div>
    </div>

  
	</div>

      </div>
    </div>

    
  
  </section>-->

  <!--<section id="icon-features" class="section--feature-icon bg-white p-0">
  <div class="col-11 col-md-10 col-xl-11 mx-auto">
  <ul class="row list-unstyled pt-3 justify-content-center">
  	<li class="col-sm-6 col-lg-5 mb-2 d-flex align-items-center">
		<div class="feature-icon d-flex align-items-center">
			<div class="mr-3"><i class="icon"><img src="assets/imgs/ic-main-1.png" width="50"></i></div>
			<div>
				<p class="text-black-50 mb-0">•  UL - allow u sell in USA and Canada.</p>
				<p class="text-black-50 mb-0">•  TUV - CE safety regulation and standard product<br class="ml-2">( EMC and MD )</p>
			</div>
		</div>
	</li>
	<li class="col-sm-6 col-lg-4 mb-2 d-flex align-items-center">
		<div class="feature-icon d-flex align-items-center">
			<div class="mr-3"><i class="icon"><img src="assets/imgs/ic-main-2.png" width="50"></i></div>
			<div>
				<p class="text-black-50 mb-0">•  Natural Navigation<br class="ml-2">Deleted 2cm. - 5cm. </p>
			</div>
		</div>
	</li>
	<li class="col-sm-6 col-lg-5 mb-2 d-flex align-items-center">
		<div class="feature-icon d-flex align-items-center">
			<div class="mr-3"><i class="icon"><img src="assets/imgs/ic-main-3.png" width="50"></i></div>
			<div>
				<p class="text-black-50 mb-0">•  Multiple surface of floor </p>
			</div>
		</div>
	</li>
	<li class="col-sm-6 col-lg-4 mb-2 d-flex align-items-center">
		<div class="feature-icon d-flex align-items-center">
			<div class="mr-3"><i class="icon"><img src="assets/imgs/ic-main-4.png" width="50"></i></div>
			<div>
				<p class="text-black-50 mb-0">•  Dual Model<br>Automatic and Manual driving <br class="ml-2">for special areas.  </p>
			</div>
		</div>
	</li>
	<li class="col-sm-6 col-lg-5 mb-2 d-flex align-items-center">
		<div class="feature-icon d-flex align-items-center">
			<div class="mr-3"><i class="icon"><img src="assets/imgs/ic-main-5.png" width="50"></i></div>
			<div>
				<p class="text-black-50 mb-0">•  Vehicle 3D laser , 2 units of 2D <br class="ml-2">(More than 150 m. far senses)</p>
			</div>
		</div>
	</li>
	<li class="col-sm-6 col-lg-4 mb-2 d-flex align-items-center">
		<div class="feature-icon d-flex align-items-center">
			<div class="mr-3"><i class="icon"><img src="assets/imgs/ic-main-6.png" width="50"></i></div>
			<div>
				<p class="text-black-50 mb-0">•  6 hours battery life<br class="ml-2">sewage filtration circulation system</p>
			</div>
		</div>
	</li>
  </ul>
  </div>
  
  </section>
  
  <section id="product-features" class="section--feature-tabs bg-white p-0">
  
      
    
    <div class="row no-gutters">
      <div class="col-12">
        
<ul class="nav nav-tabs" id="product-feature-tabs" role="tablist">
	
	<li class="nav-item text-center">
		<a class="nav-link active" id="ecobot-scrub-75-tab" data-toggle="tab" href="#ecobot-scrub-75" role="tab" aria-controls="ecobot-scrub-75" aria-selected="true"><img src="assets/imgs/cleaning-e75.png" alt="Scrub 75"></a>
	</li>

	
	<li class="nav-item text-center">
		<a class="nav-link" id="ecobot-scrub-50-tab" data-toggle="tab" href="#ecobot-scrub-50" role="tab" aria-controls="ecobot-scrub-50" aria-selected="true"><img src="assets/imgs/cleaning-e50.png" alt="Scrub 50"></a>
	</li>

	</ul>

<div class="tab-content position-relative" id="product-feature-tab-content">
	
	<div class="tab-pane fade active show" id="ecobot-scrub-75" role="tabpanel" aria-labelledby="ecobot-scrub-75-tab">
		<div class="feature-content-container">
			<div class="row no-gutters w-100 h-100">
				<div class="col-11 col-md-10 col-xl-8 mx-auto ml-md-6 mr-md-auto">
					<div class="feature-content">
						<h1 class="feature-heading"><img src="assets/imgs/cleaning-e75.png" alt="Scrub 75"></h1>
						<a href="ecobot-scrub-75.php" class="btn btn-lg btn-secondary">Learn More</a>
					</div>
				</div>
			</div>

			
			<div class="feature-icons-bar">

				
				<div class="feature-icon">
					
					<div><i class="ic-ft-01"></i></div>
						<p class="text-white">Corner & Edge-to-Edge Cleaning</p>
				</div>

				
				<div class="feature-icon">
					
					<div><i class="ic-ft-02"></i></div>

						<p class="text-white">Operate in Auto or Manual Mode</p>
				</div>

				
				<div class="feature-icon">
					
					<div><i class="ic-ft-03"></i></div>

						<p class="text-white">6 Hours Li-ion Battery</p>
				</div>

				
				<div class="feature-icon">
					
					<div><i class="ic-ft-04"></i></div>

						<p class="text-white">4-Stage Water Filtration</p>
				</div>

				
				<div class="feature-icon">
					
					<div><i class="ic-ft-05"></i></div>

						<p class="text-white">Alerts & Reports</p>
				</div>

				
				<div class="feature-icon">
					
					<div><i class="ic-ft-06"></i></div>

						<p class="text-white">Class-Leading Speed & Coverage Area</p>
				</div>

				
				<div class="feature-icon">
					
					<div><i class="ic-ft-07"></i></div>

						<p class="text-white">Autonomous Mapping</p>
				</div>

				
			</div>

					</div>
		<img src="assets/imgs/ecobot-scrub-75-advanced-navigation-1920x810.jpg" alt="Ecobot Scrub 75" class="img-fluid">
	</div>

	
	<div class="tab-pane fade " id="ecobot-scrub-50" role="tabpanel" aria-labelledby="ecobot-scrub-50-tab">
		<div class="feature-content-container">
			<div class="row no-gutters w-100 h-100">
				<div class="col-11 col-md-10 col-xl-8 mx-auto ml-md-6 mr-md-auto">
					<div class="feature-content">
						<h1 class="feature-heading"><img src="assets/imgs/cleaning-e50.png" alt="Scrub 50"></h1>
						<a href="ecobot-scrub-50.php" class="btn btn-lg btn-secondary">Learn More</a>
					</div>
				</div>
			</div>

			
			<div class="feature-icons-bar">

				
				<div class="feature-icon">
					
					<div><i class="ic-ft-01"></i></div>

										<p class="text-white">Corner & Edge-to-Edge Cleaning</p>
				</div>

				
				<div class="feature-icon">
					
					<div><i class="ic-ft-02"></i></div>

										<p class="text-white">Advanced Navigation</p>
				</div>

				
				<div class="feature-icon">
					
					<div><i class="ic-ft-03"></i></div>


						<p class="text-white">2-3 Hours Li-ion Battery</p>	
				</div>

				
				<div class="feature-icon">
					
					<div><i class="ic-ft-04"></i></div>

										<p class="text-white">4-Stage Water Filtration</p>
				</div>

				
				<div class="feature-icon">
					
					<div><i class="ic-ft-05"></i></div>

										<p class="text-white">Alerts & Reports</p>
				</div>

				
				<div class="feature-icon">
					
					<div><i class="ic-ft-06"></i></div>

										<p class="text-white">Class-Leading Speed & Coverage Area</p>
				</div>

				
				<div class="feature-icon">
					
					<div><i class="ic-ft-07"></i></div>

										<p class="text-white">Autonomous Mapping</p>
				</div>

				
			</div>

					</div>
		<img src="assets/imgs/ecobot-scrub-50-advanced-navigation-1920x810.jpg" alt="Ecobot Scrub 50" class="img-fluid">
	</div>

	</div>      </div>
    </div>

    
  
  </section>
-->
  <!-- 
  <section id="engineered-simplicity" class="section--text-block-image bg-white p-0">
  
      
    
    <div class="row no-gutters">
      <div class="col-12">
        
<div class="row no-gutters">
	<div class="col-12 col-lg-6 order-lg-2">
		<img class="img-fit" src="assets/imgs/engineered-simplicity-easy-to-control-app.jpg" alt="Engineered Simplicity Easy To Control App">
	</div>
	<div class="col-12 col-lg-5 col-xl-4 m-auto order-lg-1">
		<div class="d-flex flex-column py-4 py-sm-5 px-4 px-sm-5 px-lg-0">
			<h1 class="section-title font-weight-bold">Engineered<br>Simplicity</h1>
        	<p class="mt-2">There is nothing like cutting-edge technology made simple. Designed with the input from operators of cleaning service providers with more than 100 combined years of operational experience, our user-friendly tablet interface caters to cleaning operators from diverse educational backgrounds of all levels and age-groups.</p>
		</div>
	</div>
</div>      </div>
    </div>

    
  
  </section>
  -->
  
  <section id="product-advantages" class="section--shape-blocks py-6">
  
      
    
    <div class="container-fluid">
      <div class="row">
        <div class="col-11 mx-auto">
          
<h3 class="font-weight-semibold h3 text-center mb-5">ทำไมต้องเลือกเรา</h3>


<div class="row no-gutters pt-4">
	<div class="col-12 mx-auto">

		<div class="shape-row shape-row-top">
		<div class="shape">
			<div class="shape-icon">
				
					<div class="fa-7x">
						<span class="fa-layers fa-fw">
							<i class="fas fa-circle shape-icon-outer" data-fa-transform="grow-6"></i>
	    					<i class="fas fa-circle text-secondary"></i>
							<i class="far fa-heart"></i>
						</span>
					</div>

							</div>
			<div class="shape-content">
				<div class="d-flex flex-column align-items-center justify-content-center text-center h-100">
					<div class="row">
						<div class="col-11 col-lg-12 mx-auto">
							<div class="shape-description">
																<h4 class="font-weight-lite text-white text-uppercase mb-2 ">มาตรฐาน</h4>
								<p class="text-center text-white mb-2">สำหรับเราลูกค้าทุกท่านสำคัญเสมอ เราจึงออกแบบบริการให้เป็นมาตรฐานเดียวกันทั้งหมด เพราะเราทราบดี ถึงความจำเป็นเร่งด่วนของปัญหาท่อตัน หรือปัญหาในการระบายน้ำที่เกิดขึ้นกับลูกค้า เราจึงคิดเสมอว่า ไม่มีงานไหนใหญ่ หรือเล็กเกินไปสำหรับเรา</p>							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		
		<div class="shape">
			<div class="shape-icon">
				
					<div class="fa-7x">
						<span class="fa-layers fa-fw">
							<i class="fas fa-circle shape-icon-outer" data-fa-transform="grow-6"></i>
	    					<i class="fas fa-circle text-secondary"></i>
							<i class="far fa-user"></i>
						</span>
					</div>

							</div>
			<div class="shape-content">
				<div class="d-flex flex-column align-items-center justify-content-center text-center h-100">
					<div class="row">
						<div class="col-11 col-lg-12 mx-auto">
							<div class="shape-description">
																<h4 class="font-weight-lite text-white text-uppercase mb-2 ">พนักงานมืออาชีพ</h4>
								<p class="text-center text-white mb-2">นอกจากทีมงานที่มากประสบการณ์ในงานระบบสุขาภิบาล และการแก้ท่อตัน เรายังมุ่งเน้นให้ทีมงานได้รับการฝึกอบรมเพิ่มเติมทั้งจากภายใน และ ภายนอก รวมถึงความสุภาพในการให้บริการ และ การแต่งกายตามมาตรฐานความปลอดภัย สิ่งเหล่านี้คือความใส่ใจของเราที่ต้องการส่งมอบให้กับลูกค้าทุกท่าน</p>							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		
		<div class="shape">
			<div class="shape-icon">
				
					<div class="fa-7x">
						<span class="fa-layers fa-fw">
							<i class="fas fa-circle shape-icon-outer" data-fa-transform="grow-6"></i>
	    					<i class="fas fa-circle text-secondary"></i>
							<i class="far fa-search"></i>
						</span>
					</div>

							</div>
			<div class="shape-content">
				<div class="d-flex flex-column align-items-center justify-content-center text-center h-100">
					<div class="row">
						<div class="col-11 col-lg-12 mx-auto">
							<div class="shape-description">
																<h4 class="font-weight-lite text-white text-uppercase mb-2 ">ตรวจเช็คหน้างานฟรี</h4>
								<p class="text-center text-white mb-2">บริการตรวจเช็คหน้างานของเรา เพื่อให้ เรา รวมถึง ลูกค้าทราบถึงปัญหาที่แท้จริงไปพร้อมกัน และสามารถวางแผนในการแก้ไขปัญหาได้อย่างถูกต้อง เพราะสาเหตุที่แท้จริงของปัญหาท่อตัน อาจจะมีมากกว่าที่คิด เราจึงไม่คิดค่าใช้จ่าย เพื่อความสบายใจ ให้กับลูกค้า</p>							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		</div><div class="shape-row shape-row-bottom">
		<div class="shape">
			<div class="shape-icon">
				
					<div class="fa-7x">
						<span class="fa-layers fa-fw">
							<i class="fas fa-circle shape-icon-outer" data-fa-transform="grow-6"></i>
	    					<i class="fas fa-circle text-secondary"></i>
							<i class="far fa-cogs"></i>
						</span>
					</div>

							</div>
			<div class="shape-content">
				<div class="d-flex flex-column align-items-center justify-content-center text-center h-100">
					<div class="row">
						<div class="col-11 col-lg-12 mx-auto">
							<div class="shape-description">
																<h4 class="font-weight-lite text-white text-uppercase mb-2 ">เครื่องมือที่ทันสมัย</h4>
								<p class="text-center text-white mb-2">เครื่องมือมีความสำคัญอย่างมากในการแก้ไขปัญหา เพื่อให้ปัญหานั้นจบได้ไว เราจึงคัดสรรเครื่องมือ จาก แบรนด์ชั้นนำ <b>ยี่ห้อ RIDGID</b> เพื่อให้ลูกค้ามั่นใจว่าปัญหาท่อตันนั้นได้รับการแก้ไขจบแล้วอย่างแท้จริง</p>							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		
		<div class="shape">
			<div class="shape-icon">
				
					<div class="fa-7x">
						<span class="fa-layers fa-fw">
							<i class="fas fa-circle shape-icon-outer" data-fa-transform="grow-6"></i>
	    					<i class="fas fa-circle text-secondary"></i>
							<i class="far fa-thumbs-up"></i>
						</span>
					</div>

							</div>
			<div class="shape-content">
				<div class="d-flex flex-column align-items-center justify-content-center text-center h-100">
					<div class="row">
						<div class="col-11 col-lg-12 mx-auto">
							<div class="shape-description">
																<h4 class="font-weight-lite text-white text-uppercase mb-2 ">รับประกันความพอใจ</h4>
								<p class="text-center text-white mb-2">เพราะเรามั่นใจในความเป็นมืออาชีพ เราจึงกล้ารับประกันถ้า <b>ปัญหาท่อตัน</b> หรือปัญหาอื่นใดของลูกค้าไม่หาย เรายินดีที่จะไม่คิดเงิน อีกทั้งเรายังมีบริการส่งมอบไฟล์ภาพ และ วิดีโอ ก่อน และ หลังการแก้ไข เพื่อความมั่นใจของลูกค้าอีกด้วย</p>							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		
		<!--<div class="shape">
			<div class="shape-icon">
				
					<div class="fa-7x">
						<span class="fa-layers fa-fw">
							<i class="fas fa-circle shape-icon-outer" data-fa-transform="grow-6"></i>
	    					<i class="fas fa-circle text-secondary"></i>
							<i class="icn-sv-06"></i>
						</span>
					</div>

							</div>
			<div class="shape-content">
				<div class="d-flex flex-column align-items-center justify-content-center text-center h-100">
					<div class="row">
						<div class="col-11 col-lg-12 mx-auto">
							<div class="shape-description">
																<h4 class="font-weight-bold text-white text-uppercase mb-2 ">MAINTENANCE</h4>
								<p class="text-center text-white mb-2 px-5">We work with robot manufacturers to provide after sales services for the robots</p>							</div>
						</div>
					</div>
				</div>
			</div>
		</div>-->

		</div>
	</div>
</div>        </div>
      </div>
    </div>

    
  
  </section>




</main>

<footer>

	
	
<section id="request-demo" class="bg-blue">
	<div class="row no-gutters">

		
		<div class="col-12 col-xl-6 order-2 order-xl-1">
			<div class="bg-image bg-primary py-5 py-md-6 px-4 px-sm-5 px-xl-6 h-100">
				<!--<h2 class="font-weight-bold text-white text-center text-sm-left text-uppercase mb-4">WE'RE IN THAILAND</h2>-->
				<div class="d-flex flex-column h-100 justify-content-center">
						<div class="my-3">
							<div class="d-flex">

								<div class="ml-4">
									<p class="h5 text-white text-uppercase mb-4">Contact us</p>
									<ul class="list-unstyled mb-0">
										<li class="h6 text-light">K-Wiz Solution</li>
										<li class="h6 text-light">69/198 ม.10 ต.บางแม่นาง อ.บางใหญ่ จ.นนทบุรี 11140,<br>
										Tel : <a href="tel:020237331" class="text-white">02-0237331</a> , Hotline : <a href="tel:0814673826" class="text-white">081-4673826</a></li>
									</ul>
									<div class="mt-3">
									<!--<a id="btn-demo-ft" href="javascript:void(0)" class="btn btn-danger btn-md" data-toggle="modal" data-target="#booking-form">REQUEST A DEMO</a>-->
									<a href="javascript:void(0)" data-fancybox data-type="iframe" data-src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3873.2098860186943!2d100.37524551529813!3d13.886394797900467!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e28e2385f286ad%3A0xb53a46fb31f837de!2sK-Wiz+Solution+Co.%2CLtd.!5e0!3m2!1sen!2sth!4v1526283996151" class="btn btn-secondary btn-md">เปิดแผนที่</a>
									</div>
								</div>
							</div>
						</div>
						
					</div>
								
			</div>
			<div class="gmap">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3873.2098860186943!2d100.37524551529813!3d13.886394797900467!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e28e2385f286ad%3A0xb53a46fb31f837de!2sK-Wiz+Solution+Co.%2CLtd.!5e0!3m2!1sen!2sth!4v1526283996151" style="border:0" allowfullscreen="" width="100%" height="780" frameborder="0"></iframe>
			</div>
		</div>

		
		<div class="col-12 col-lg-9 col-xl-4 order-1 order-xl-2 m-auto">
			<div class="d-flex flex-column py-5 py-xl-6 px-4 px-sm-5 px-md-6 px-lg-4">
								<!--<h1 class="font-weight-bold text-primary text-center text-uppercase mb-4">Request for Appointment</h1>-->
								<p class="font-weight-normal text-white text-center mb-4"><big>ติดต่อสอบถามบริการ</big></p>
				<div role="form" class="wpcf7" lang="en-US" dir="ltr">
<form action="/#home" method="post" class="wpcf7-form" novalidate="novalidate">
<?php include("incs/contactform.html"); ?>
</form>
				</div>
			</div>
		</div>

	</div>
</section>


<!-- footer -->
<?php include("incs/footer.html"); ?>
<!-- /footer -->
	
</footer>

<?php include("incs/js_footer.html"); ?>
</body>
</html>