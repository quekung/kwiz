<?php
//header("HTTP/1.1 301 Moved Permanently"); 
//header("Location: https://www.smarttechcleaning.com/"); 
//exit();
?>
<!DOCTYPE html>
<html lang="en-US" class="no-js">
<head>
<!-- header-top -->
<?php include("incs/header-top.html"); ?>
<!-- /header-top -->
</head>

<body class="home page-template-default page page-id-1">
<!-- header -->
<?php include("incs/header.html"); ?>
<!-- /header -->
  <div class="navbar-pin"></div>

  <main id="ecobot-scrub-75">

  
<section class="hero-subpage hero-center" style="background-image: url('assets/imgs/hero-ecobot-scrub-75-1920x680.jpg');">

	<div class="hero-title">
		<div class="container-fluid">
			<div class="row">
				<div class="col-11 mx-auto">
					<div class="d-flex flex-column align-items-center align-items-xl-start">
						<h1 class="feature-heading mb-2"><img src="assets/imgs/cleaning-e75.png" alt="Scrub 75"></h1>
						<p class="h3 text-center text-md-left text-white m-0">TAKES THE<br>
GUESSWORK OUT<br>
OF CLEANING</p>					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="hero-overlay"></div>

</section>



  <?php /*?><section id="scrub-75-intro" class="section--text-block bg-white p-0 pt-5 pt-md-6">
  
      
    
    <div class="container-fluid">
      <div class="row">
        <div class="col-11 mx-auto">
          
<div class="row no-gutters">
	<div class="col-12 mx-auto">
		<div class="text-center">
			<h1 class="font-weight-bold text-primary text-center text-uppercase mb-4">New Class Leader in Autonomous Cleaning</h1>		</div>
	</div>
		<div class="col-12 col-md-10 col-xl-8 mx-auto">
		<div class="text-center">
			<p class="text-center mb-4">The ECOBOT Scrub 75 is the new class leader in autonomous cleaning robotics. Gaussian Robotics worked closely with top cleaning service providers and facility owners to develop the Scrub 75 to allow it to clean corners, be small yet efficient, and dual purpose usage.</p>
		</div>
	</div>
	</div>        </div>
      </div>
    </div>

    
  
  </section>

  
  <section id="scrub-75-video" class="section--video-block bg-white p-0 pb-5 pb-md-6 pt-3">
  
      
    
    <div class="container-fluid">
      <div class="row">
        <div class="col-11 mx-auto">
          

<article class="video-single">
	<div class="row no-gutters">
		<div class="col-12 col-md-10 col-xl-8 mx-auto">
			<a title="Click to Play ECOBOT Scrub 75 &#8211; New Class Leader in Autonomous Cleaning" class="video-thumbnail" href="https://www.youtube.com/watch?v=E5J8zZaWw_E" data-lity>
				<i class="fas fa-fw fa-2x fa-play"></i>
				<img alt="ECOBOT Scrub 75 &#8211; New Class Leader in Autonomous Cleaning" class="img-fit" src="assets/imgs/ecobot-scrub-75-the-new-class-leader.jpg">
			</a>
		</div>
	</div>
</article>


        </div>
      </div>
    </div>

    
  
  </section>
<?php */?>
  
  <section id="scrub-75-productive" class="section--text-block-icons-image bg-white p-0">
  
      
    
    <div class="row no-gutters">
      <div class="col-12">
        
<div class="row no-gutters">
	<div class="col-12 col-lg-6 order-lg-2">
					<img class="img-fit" src="assets/imgs/ecobot-scrub-75-front.jpg" alt="Ecobot Scrub 75 Front">
			</div>
	<div class="col-12 col-lg-5 col-xl-4 m-auto order-lg-1">
		<div class="d-flex flex-column py-4 py-sm-5 px-4 px-sm-5 px-lg-0">
			<h1 class="section-title font-weight-bold text-primary text-uppercase"><span class="text-tertiary"></span> Productive</h1>
        	<p class="mt-2 mb-2">The Scrub 75, using its 3 sensor systems, LIDAR, 3-D depth cameras and Ultrasonic sensors, works to attain a 99% consistent coverage area.  This eliminates the need for manual touch ups and keeps your staff working on more important matters.</p>
<div class="d-flex flex-column py-3 py-sm-0 ml-2">
								<div class="d-flex flex-row align-items-center justify-content-center py-3">
						
						<div class="d-flex fa-2x">
							<span class="fa-layers fa-fw">
								<i class="fas fa-hexagon text-primary" data-fa-transform="grow-9.5 rotate-90"></i>
								<i class="fa-inverse fal fa-battery-full text-white" data-fa-transform="shrink-4"></i>
							</span>
						</div>

						
						<div class="ml-4 mr-auto">

														<p class="font-weight-semibold text-dark">6hr Lithium Ion Battery</p>
						</div>
					</div>
			
			
								<div class="d-flex flex-row align-items-center justify-content-center py-3">
						
						<div class="d-flex fa-2x">
							<span class="fa-layers fa-fw">
								<i class="fas fa-hexagon text-primary" data-fa-transform="grow-9.5 rotate-90"></i>
								<i class="fa-inverse fal fa-tachometer-alt-fastest text-white" data-fa-transform="shrink-4"></i>
							</span>
						</div>

						
						<div class="ml-4 mr-auto">

														<p class="font-weight-semibold text-dark">Up to 3x Faster Than Competitors</p>
						</div>
					</div>
			
			
								<div class="d-flex flex-row align-items-center justify-content-center py-3">
						
						<div class="d-flex fa-2x">
							<span class="fa-layers fa-fw">
								<i class="fas fa-hexagon text-primary" data-fa-transform="grow-9.5 rotate-90"></i>
								<i class="fa-inverse fal fa-vector-square text-white" data-fa-transform="shrink-4"></i>
							</span>
						</div>

						
						<div class="ml-4 mr-auto">

														<p class="font-weight-semibold text-dark">Corner & Edge-to-Edge Cleaning</p>
						</div>
					</div>
			
			</div>		</div>
	</div>
</div>      </div>
    </div>

    
  
  </section>

  
  <section id="scrub-75-simple" class="section--text-block-icons-image bg-white p-0">
  
      
    
    <div class="row no-gutters">
      <div class="col-12">
        
<div class="row no-gutters">
	<div class="col-12 col-lg-6 order-lg-1">
					<img class="img-fit" src="assets/imgs/ecobot-scrub-75-side.jpg" alt="Ecobot Scrub 75 Side">
			</div>
	<div class="col-12 col-lg-5 col-xl-4 m-auto order-lg-2">
		<div class="d-flex flex-column py-4 py-sm-5 px-4 px-sm-5 px-lg-0">
			<h1 class="section-title font-weight-bold text-primary text-uppercase"><span class="text-tertiary"></span> Simple</h1>
        	<p class="mt-2 mb-2">Scrub 75 takes the guesswork out of autonomous cleaning, with its ability to map over 100,000 m<sup>2</sup> of coverage in a single map, as well as an Alert &amp; Report interface to warn you of areas that were blocked or inaccessible for cleaning. With a touch of the button, the Scrub 75 switches to manual operation for rapid deploy or inaccessible areas.</p>
<div class="d-flex flex-column py-3 py-sm-0 ml-2">
								<div class="d-flex flex-row align-items-center justify-content-center py-3">
						
						<div class="d-flex fa-2x">
							<span class="fa-layers fa-fw">
								<i class="fas fa-hexagon text-primary" data-fa-transform="grow-9.5 rotate-90"></i>
								<i class="fa-inverse fal fa-exchange-alt text-white" data-fa-transform="shrink-4"></i>
							</span>
						</div>

						
						<div class="ml-4 mr-auto">

														<p class="font-weight-semibold text-dark">True Dual Purpose</p>
						</div>
					</div>
			
			
								<div class="d-flex flex-row align-items-center justify-content-center py-3">
						
						<div class="d-flex fa-2x">
							<span class="fa-layers fa-fw">
								<i class="fas fa-hexagon text-primary" data-fa-transform="grow-9.5 rotate-90"></i>
								<i class="fa-inverse fal fa-bell text-white" data-fa-transform="shrink-4"></i>
							</span>
						</div>

						
						<div class="ml-4 mr-auto">

														<p class="font-weight-semibold text-dark">Alert & Report Interface</p>
						</div>
					</div>
			
			
								<div class="d-flex flex-row align-items-center justify-content-center py-3">
						
						<div class="d-flex fa-2x">
							<span class="fa-layers fa-fw">
								<i class="fas fa-hexagon text-primary" data-fa-transform="grow-9.5 rotate-90"></i>
								<i class="fa-inverse fal fa-map-marked text-white" data-fa-transform="shrink-4"></i>
							</span>
						</div>

						
						<div class="ml-4 mr-auto">

														<p class="font-weight-semibold text-dark">Easy and Fast Mapping System</p>
						</div>
					</div>
			
			</div>		</div>
	</div>
</div>      </div>
    </div>

    
  
  </section>

  
  <section id="scrub-75-efficient" class="section--text-block-icons-image bg-white p-0">
  
      
    
    <div class="row no-gutters">
      <div class="col-12">
        
<div class="row no-gutters">
	<div class="col-12 col-lg-6 order-lg-2">
					<img class="img-fit" src="assets/imgs/ecobot-scrub-75-back.jpg" alt="Ecobot Scrub 75 Back">
			</div>
	<div class="col-12 col-lg-5 col-xl-4 m-auto order-lg-1">
		<div class="d-flex flex-column py-4 py-sm-5 px-4 px-sm-5 px-lg-0">
			<h1 class="section-title font-weight-bold text-primary text-uppercase"><span class="text-tertiary"></span> Efficient</h1>
        	<p class="mt-2 mb-2">With the Scrub 75, the four stage filtration system can cut down your monthly water usage by up to 6,000 litres. With class leading coverage of 3,000 m<sup>2</sup>/hr the Scrub 75 will save you both time and money when it comes to your floor cleaning.</p>
<div class="d-flex flex-column py-3 py-sm-0 ml-2">
								<div class="d-flex flex-row align-items-center justify-content-center py-3">
						
						<div class="d-flex fa-2x">
							<span class="fa-layers fa-fw">
								<i class="fas fa-hexagon text-primary" data-fa-transform="grow-9.5 rotate-90"></i>
								<i class="fa-inverse fal fa-arrows text-white" data-fa-transform="shrink-4"></i>
							</span>
						</div>

						
						<div class="ml-4 mr-auto">

														<p class="font-weight-semibold text-dark">Class Leading Coverage</p>
						</div>
					</div>
			
			
								<div class="d-flex flex-row align-items-center justify-content-center py-3">
						
						<div class="d-flex fa-2x">
							<span class="fa-layers fa-fw">
								<i class="fas fa-hexagon text-primary" data-fa-transform="grow-9.5 rotate-90"></i>
								<i class="fa-inverse fal fa-recycle text-white" data-fa-transform="shrink-4"></i>
							</span>
						</div>

						
						<div class="ml-4 mr-auto">

														<p class="font-weight-semibold text-dark">4 Stage Filtration System</p>
						</div>
					</div>
			
			
								<div class="d-flex flex-row align-items-center justify-content-center py-3">
						
						<div class="d-flex fa-2x">
							<span class="fa-layers fa-fw">
								<i class="fas fa-hexagon text-primary" data-fa-transform="grow-9.5 rotate-90"></i>
								<i class="fa-inverse fal fa-wrench text-white" data-fa-transform="shrink-4"></i>
							</span>
						</div>

						
						<div class="ml-4 mr-auto">

														<p class="font-weight-semibold text-dark">Quick and Easy to Maintain</p>
						</div>
					</div>
			
			</div>		</div>
	</div>
</div>      </div>
    </div>

    
  
  </section>


  <section id="scrub-75-specifications " class="section--specification-list bg-white py-5">
  
      
    
    <div class="container-fluid">
      <div class="row">
        <div class="col-11 mx-auto">
          
<div class="row no-gutters">
	<?php /*?><div class="col-12 col-md-11 mx-auto">
		<h1 class="font-weight-bold text-primary text-center text-uppercase mb-4">Technical Specifications</h1>	</div><?php */?>
		<div class="col-12 col-lg-10 col-xl-6 order-2 order-xl-1 mx-auto">
		<div class="specs-list">

		
		<div class="spec">
			<div class="row no-gutters">
				<div class="col-12 col-sm-6">
										<p class="h6 text-primary">Dimensions (MM)</p>
									</div>

				<div class="col-12 col-sm-4 col-xl-4 ml-auto">

				<p class="text-left text-sm-center mb-0">1290(L) X 760(W) X 1120(H)</p>
				</div>
			</div>
		</div>

		
		<div class="spec">
			<div class="row no-gutters">
				<div class="col-12 col-sm-6">
										<p class="h6 text-primary">Scrubbing Width</p>
									</div>

				<div class="col-12 col-sm-4 col-xl-4 ml-auto">

				<p class="text-left text-sm-center mb-0">747 MM</p>
				</div>
			</div>
		</div>

		
		<div class="spec">
			<div class="row no-gutters">
				<div class="col-12 col-sm-6">
										<p class="h6 text-primary">Cleaning Efficiency</p>
									</div>

				<div class="col-12 col-sm-4 col-xl-4 ml-auto">

				<p class="text-left text-sm-center mb-0">Up to 3000 M<sup>2</sup>/H</p>
				</div>
			</div>
		</div>

		
		<div class="spec">
			<div class="row no-gutters">
				<div class="col-12 col-sm-6">
										<p class="h6 text-primary">Batteries</p>
									</div>

				<div class="col-12 col-sm-4 col-xl-4 ml-auto">

				<p class="text-left text-sm-center mb-0">Li-Ion</p>
				</div>
			</div>
		</div>

		
		<div class="spec">
			<div class="row no-gutters">
				<div class="col-12 col-sm-6">
										<p class="h6 text-primary">Average Run-Time</p>
									</div>

				<div class="col-12 col-sm-4 col-xl-4 ml-auto">

				<p class="text-left text-sm-center mb-0">4-6 hours</p>
				</div>
			</div>
		</div>

		
		<div class="spec">
			<div class="row no-gutters">
				<div class="col-12 col-sm-6">
										<p class="h6 text-primary">Total Power</p>
									</div>

				<div class="col-12 col-sm-4 col-xl-4 ml-auto">

				<p class="text-left text-sm-center mb-0">2000-2400 W</p>
				</div>
			</div>
		</div>

		
		<div class="spec">
			<div class="row no-gutters">
				<div class="col-12 col-sm-6">
										<p class="h6 text-primary">Voltage</p>
									</div>

				<div class="col-12 col-sm-4 col-xl-4 ml-auto">

				<p class="text-left text-sm-center mb-0">24 V</p>
				</div>
			</div>
		</div>

		
		<div class="spec">
			<div class="row no-gutters">
				<div class="col-12 col-sm-6">
										<p class="h6 text-primary">Rated Driving Motor Power</p>
									</div>

				<div class="col-12 col-sm-4 col-xl-4 ml-auto">

				<p class="text-left text-sm-center mb-0">280 W</p>
				</div>
			</div>
		</div>

		
		<div class="spec">
			<div class="row no-gutters">
				<div class="col-12 col-sm-6">
										<p class="h6 text-primary">Rated Brush Motor Power</p>
									</div>

				<div class="col-12 col-sm-4 col-xl-4 ml-auto">

				<p class="text-left text-sm-center mb-0">3 X 150 W</p>
				</div>
			</div>
		</div>

		
		<div class="spec">
			<div class="row no-gutters">
				<div class="col-12 col-sm-6">
										<p class="h6 text-primary">Brush Rotation Speed</p>
									</div>

				<div class="col-12 col-sm-4 col-xl-4 ml-auto">

				<p class="text-left text-sm-center mb-0">Up to 270 RPM</p>
				</div>
			</div>
		</div>

		
		<div class="spec">
			<div class="row no-gutters">
				<div class="col-12 col-sm-6">
										<p class="h6 text-primary">Rated Water Suction Motor Power</p>
									</div>

				<div class="col-12 col-sm-4 col-xl-4 ml-auto">

				<p class="text-left text-sm-center mb-0">500 W</p>
				</div>
			</div>
		</div>

		
		<div class="spec">
			<div class="row no-gutters">
				<div class="col-12 col-sm-6">
										<p class="h6 text-primary">Maximum Water Suction Pressure</p>
									</div>

				<div class="col-12 col-sm-4 col-xl-4 ml-auto">

				<p class="text-left text-sm-center mb-0">18.18 kPa</p>
				</div>
			</div>
		</div>

		
		<div class="spec">
			<div class="row no-gutters">
				<div class="col-12 col-sm-6">
										<p class="h6 text-primary">Clean Water Tank</p>
									</div>

				<div class="col-12 col-sm-4 col-xl-4 ml-auto">

				<p class="text-left text-sm-center mb-0">65 L</p>
				</div>
			</div>
		</div>

		
		<div class="spec">
			<div class="row no-gutters">
				<div class="col-12 col-sm-6">
										<p class="h6 text-primary">Waste Water Tank</p>
									</div>

				<div class="col-12 col-sm-4 col-xl-4 ml-auto">

				<p class="text-left text-sm-center mb-0">55 L</p>
				</div>
			</div>
		</div>

		
		<div class="spec">
			<div class="row no-gutters">
				<div class="col-12 col-sm-6">
					<div class="d-flex flex-column justify-content-center h-100">					<p class="h6 text-primary">Safety System</p>
					</div>				</div>

				<div class="col-12 col-sm-4 col-xl-4 ml-auto">

				<div class="d-flex flex-column"><p class="text-left text-sm-center mb-0">Laser Scanner, Depth Camera</p><p class="text-left text-sm-center mb-0">Ultrasonic, Touch Bumpers</p></div>
				</div>
			</div>
		</div>

		
		<div class="spec">
			<div class="row no-gutters">
				<div class="col-12 col-sm-6">
										<p class="h6 text-primary">Speed</p>
									</div>

				<div class="col-12 col-sm-4 col-xl-4 ml-auto">

				<p class="text-left text-sm-center mb-0">0-4 KM/H</p>
				</div>
			</div>
		</div>

		
		<div class="spec">
			<div class="row no-gutters">
				<div class="col-12 col-sm-6">
										<p class="h6 text-primary">Noise</p>
									</div>

				<div class="col-12 col-sm-4 col-xl-4 ml-auto">

				<p class="text-left text-sm-center mb-0">55-70 dBA</p>
				</div>
			</div>
		</div>

		
		</div>
	</div>
		<div class="col-12 col-sm-9 col-md-8 col-lg-6 col-xl-4 order-1 order-xl-2 m-auto">
		<img src="assets/imgs/ecobot-scrub-75-line-art.png" alt="Ecobot Scrub 75 Line Art" class="img-fluid mb-4 mb-xl-0">
	</div>
	</div>        </div>
      </div>
    </div>

    
  
  </section>



</main>

<footer>

	
	
<section id="request-demo" class="bg-blue">
	<div class="row no-gutters">

		<div class="col-12 col-lg-6 col-xl-6 order-2 order-xl-1">
			<div class="bg-pd  py-5 py-md-6 px-4 px-sm-5 px-xl-6 h-100">
				
				<div class="d-flex flex-column h-100 justify-content-center">
					<figure class="text-center"><img src="assets/imgs/ecobot-scrub-75.png" alt="Ecobot Scrub 75"></figure>
				</div>
			</div>
		</div>
		
		<div class="col-12 col-lg-6 col-xl-5 order-1 order-xl-2 m-auto">
			<div class="d-flex flex-column py-5 py-xl-6 px-4 px-sm-5 px-md-6 px-lg-4">
								<h1 class="font-weight-bold text-primary text-center text-uppercase mb-4">Request for Appointment</h1>
								<p class="font-weight-normal text-white mb-4">For more information about the Ecobot, or to see the Ecobot in action, please fill out the form below and we will get in touch with you!</p>
				<div role="form" class="wpcf7" lang="en-US" dir="ltr">
<form action="/#ecobot-scrub-75" method="post" class="wpcf7-form" novalidate="novalidate">
<?php include("incs/contactform.html"); ?>
</form>
				</div>
			</div>
		</div>

	</div>
</section>


	
	<!-- footer -->
<?php include("incs/footer.html"); ?>
<!-- /footer -->
</footer>

<?php include("incs/js_footer.html"); ?>
</body>
</html>