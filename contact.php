<?php
//header("HTTP/1.1 301 Moved Permanently"); 
//header("Location: https://www.smarttechcleaning.com/"); 
//exit();
?>
<!DOCTYPE html>
<html lang="en-US" class="no-js">
<head>
<!-- header-top -->
<?php include("incs/header-top.html"); ?>
<!-- /header-top -->
</head>

<body class="home page-template-default page page-id-1">
<!-- header -->
<?php include("incs/header.html"); ?>
<!-- /header -->
  <div class="navbar-pin"></div>



  
<?php /*?><section class="hero-subpage hero-center" style="background-image: url('assets/imgs/hero-ecobot-scrub-50-1920x680.jpg');">

	<div class="hero-title">
		<div class="container-fluid">
			<div class="row">
				<div class="col-11 mx-auto">
					<div class="d-flex flex-column align-items-center align-items-xl-start">
						<h1 class="feature-heading mb-2 text-white">Contact</h1>
						<p class="h3 text-center text-md-left text-white m-0">For more information<br>about the Ecobot</p>					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="hero-overlay"></div>

</section><?php */?>
<section id="carousel" style="margin-bottom: -30px;z-index: 99">
  
	
      <div class="slide-wrapper">
        <div class="slide-content">
         
		 <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2" class="active"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item">
	<div class="container row mx-auto">
		  <figure class="hidden-xs col-sm-7 d-flex justify-content-center justify-content-sm-start align-items-end mb-0">
		  	<img class="col-sm-8 img-bottom" src="assets/imgs/product-banner1.png">
		  </figure>
		  <div class="col-sm-5 d-flex flex-column align-items-start justify-content-center h-100">
			  <h1 class="slide-title font-weight-bold text-white mb-2"><img src="assets/imgs/logo-white.png" height="30"><span class="text-hide"></span></h1>
			  <p class="slide-caption font-weight-light text-white mb-4">Meet our Robots<br>Full range of Autonomous Cleaning<br>Robots Services available</p>
				<div class="d-flex flex-column flex-sm-row">
					<a href="javascript:void(0)" class="btn btn-secondary btn-lg af-none" data-toggle="modal" data-target="#booking-form">REQUEST A DEMO</a>
				  </div>
		  </div>
	  </div>
    </div>
    <div class="carousel-item">
      <div class="container row mx-auto">

		  <div class="col-sm-6 d-flex flex-column align-items-start justify-content-center h-100">
			  <h1 class="slide-title font-weight-bold text-white mb-2">Our Services</h1>
			  <p class="slide-caption font-weight-light text-white mb-4">
Whether you are a company interested to integrate
robotics salutions into your business or a robotics
manufacturer looking for robotics experts to help
distribute your products</p>
				<p class="slide-caption font-weight-light text-white mb-4">
					<big>we are here to work with you!</big>
				  </p>
		  </div>
	  </div>
    </div>
	
    <div class="carousel-item active">
      <div class="container row mx-auto">
		  <figure class="hidden-xs col-sm-7 d-flex justify-content-center justify-content-sm-start align-items-end mb-0">
		  	<img class="col-sm-12" src="assets/imgs/product-banner3.png">
		  </figure>
		  <div class="col-sm-5 d-flex flex-column align-items-start justify-content-center h-100">
			  <h1 class="slide-title font-weight-bold text-white mb-2"><img src="assets/imgs/logo-white.png" height="30"><span class="text-hide"></span></h1>
			  <p class="slide-caption font-weight-light text-white mb-4">Contact<br>For more infomation<br>about the Ecobot</p>
	
		  </div>
	  </div>
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
        </div>
        <!--<a class="scroll-prompt" href="javascript:void(0)">
          <span class="mouse"></span>
          <i class="far fa-chevron-down text-white"></i>
        </a>-->
                  
        <video autoplay muted loop class="slide-video" poster="assets/imgs/scrub-50-wall-detecting-sensors.jpg">
          <source src="assets/imgs/1123878774.mp4" type="video/mp4">
          Your browser does not support HTML5 video.
        </video>
                <img class="slide-bg" src="assets/imgs/scrub-50-wall-detecting-sensors.jpg" alt="Cleaning Redefined">
        <div class="slide-overlay"></div>
      </div>

  </section>



  



<footer>

	
<section id="request-demo" class="bg-blue">
	<div class="row no-gutters">

		
		<div class="col-12 col-xl-6 order-2 order-xl-1">
			<div class="bg-image bg-primary py-5 py-md-6 px-4 px-sm-5 px-xl-6 h-100">
				
				<div class="d-flex flex-column h-100 justify-content-center">
										<div class="my-3">
						<div class="d-flex">

							<div class="ml-4">
								<p class="h5 text-white text-uppercase mb-4"><img src="assets/imgs/logo-white.png" alt="RAAS PAL" height="40"></p>
								<ul class="list-unstyled mb-0">
									<li class="h6 text-light">RAAS PAL</li>
									<li class="h6 text-light">2, Soi Tiwanon 25, Intersection 25,<br>Bang Kraso, Mueang Nonthaburi,<br>Nonthaburi 11000, Thailand</li>
								</ul>
							</div>
						</div>
					</div>
										
									</div>
								
							</div>
		</div>

		
		<div class="col-12 col-lg-9 col-xl-4 order-1 order-xl-2 m-auto">
			<div class="d-flex flex-column py-5 py-xl-6 px-4 px-sm-5 px-md-6 px-lg-4">
								<p class="font-weight-normal text-white text-center mb-4"><big>For more information about the Ecobot or to see <br class="hidden-xs">
								the Ecobot in action, please ﬁll out the form below<br class="hidden-xs">
								and we will get in touch with you!</big></p>
				<div role="form" class="wpcf7" lang="en-US" dir="ltr">
<form action="/#contact" method="post" class="wpcf7-form" novalidate="novalidate">
<?php include("incs/contactform.html"); ?>
</form>
				</div>
			</div>
		</div>

	</div>
</section>


<!-- footer -->
<?php include("incs/footer.html"); ?>
<!-- /footer -->
	
</footer>

<?php include("incs/js_footer.html"); ?>
</body>
</html>